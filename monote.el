;;; monote.el --- Moritz notes utilites

;; Copyright (C) 2011 Free Software Foundation, Inc.

;; Author: Marcos G Moritz <marcosmoritz@gmail.com>
;; Version: 0.0.1
;; Package-Requires: ((ivy 0.13.0))
;; Keywords: gcloud, google, cli
;; URL: https://mo-profile.com/monote

;;; Commentary:
;; This is package to quickly create video

;; This is provides utilities to handle quick notes
;; and run it in a subprocess

(require 'ivy)

(defcustom monote--notes-directory
  "~/workspace/notes"
  "monote notes directory"
  :type 'string
  :group 'monote)

(defun monote--list ()
  (let* ((default-directory monote--notes-directory)
         (files
          (cl-remove-if
           (lambda (item)
             (or
              (nth 1 item)
              (not (string-match "^[0-9]\\{10\\}$" (nth 0 item)))))
           (directory-files-and-attributes "~/workspace/notes"))))
    (mapcar
     (lambda (x)
       `((name . ,(nth 0 x))
         (access . ,(format-time-string "%d/%m/%y"  `,(nth 5 x)))
         (change . ,(format-time-string "%d/%m/%y"  `,(nth 6 x)))
         (status . ,(format-time-string "%d/%m/%y"  `,(nth 7 x)))
         (size . ,(nth 8 x))
         (mode . ,(nth 9 x))
         (title . ,(shell-command-to-string (format "head -n 1 %s | tr -d '\n'" (nth 0 x)))))) files)))

(defun monote-find ()
  "`find-file' using ivy to search given the date and first line"
  (interactive)
  (let ((default-directory monote--notes-directory))
    (ivy-read "Select a note: "
              (mapcar (lambda (x)
                        (format "%s %s %s"
                                (alist-get 'name x)
                                (alist-get 'status x)
                                (alist-get 'title x)))
                      (monote--list))
              :require-match t
              :sort t
              :action (lambda (x) (find-file (car (split-string x)))))))

(add-to-list 'ivy-sort-functions-alist '(monote-find . string>))

(defun monote-new ()
  "Create a new buffer and file to hold notes. Name it with unix epoch"
  (interactive)
  (let* ((default-directory monote--notes-directory)
         (initial-major-mode (quote text-mode))
         ($buffer (generate-new-buffer (shell-command-to-string "echo -n $(date '+%s')"))))
    (switch-to-buffer $buffer)
    (funcall initial-major-mode)
    (setq buffer-offer-save t)
    $buffer))

(defun monote-last()
  "Opens the most recent note buffer."
  (interactive)
  (let* ((default-directory monote--notes-directory)
         (initial-major-mode (quote text-mode))
         ($buffer (shell-command-to-string "echo -n $(find . -type f -regex \"^./[0-9].*$\" | sort -nr | head -n 1)")))
    (find-file $buffer)))



(provide 'monote)
